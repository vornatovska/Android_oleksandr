package ua.oleksandr.guitar;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

public class Chords_menu extends ActionBarActivity {
   // int[] images_chords_main = {R.drawable.c, R.drawable.c_major, R.drawable.d, R.drawable.d_major, R.drawable.e, R.drawable.f, R.drawable.f_major,
   //         R.drawable.g, R.drawable.g_major, R.drawable.a, R.drawable.a_major, R.drawable.b, R.drawable.cm};
    String[] spinerChords = {"C", "C#","Cm","D","D#","Dm","E","Em","F","Fm","F#","G","Gm","G#","A","Am","A#","H","Hm"};
    ImageView imageChords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chords_menu);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinerChords);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        imageChords = (ImageView) findViewById(R.id.imageChords);

        Spinner spinnerChords = (Spinner) findViewById(R.id.spinnerChords);

        spinnerChords.setAdapter(adapter);

        spinnerChords.setAdapter(adapter);
        spinnerChords.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position) {
                    case 0:
                        imageChords.setImageResource(R.drawable.c);
                        break;
                    case 1:
                        imageChords.setImageResource(R.drawable.c_major);
                        break;
                    case 2:
                        imageChords.setImageResource(R.drawable.cm);
                        break;
                    case 3:
                        imageChords.setImageResource(R.drawable.d);
                        break;
                    case  4:
                        imageChords.setImageResource(R.drawable.d_major);
                        break;
                    case  5:
                        imageChords.setImageResource(R.drawable.dm);
                        break;
                    case 6:
                        imageChords.setImageResource(R.drawable.e);
                        break;
                    case 7:
                        imageChords.setImageResource(R.drawable.em);
                        break;
                    case 8:
                        imageChords.setImageResource(R.drawable.f);
                        break;
                    case 9:
                        imageChords.setImageResource(R.drawable.fm);
                        break;
                    case 10:
                        imageChords.setImageResource(R.drawable.f_major);
                        break;
                    case 11:
                        imageChords.setImageResource(R.drawable.g);
                        break;
                    case 12:
                        imageChords.setImageResource(R.drawable.gm);
                        break;
                    case 13:
                        imageChords.setImageResource(R.drawable.g_major);
                        break;
                    case 14:
                        imageChords.setImageResource(R.drawable.a);
                        break;
                    case 15:
                        imageChords.setImageResource(R.drawable.am);
                        break;
                    case 16:
                        imageChords.setImageResource(R.drawable.a_major);
                        break;
                    case 17:
                        imageChords.setImageResource(R.drawable.h);
                        break;
                    case 18:
                        imageChords.setImageResource(R.drawable.hm);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chords_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBackClick(View view) {
        switch (view.getId()){
            case R.id.ButtonBackSongMenu:
                Chords_menu.super.onBackPressed();
                break;
        }
    }
}