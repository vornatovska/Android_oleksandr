package ua.oleksandr.guitar;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class Songs_Russian extends ActionBarActivity implements OnClickListener {
    Map<String, Integer> mapIndexRus;
    ListView listOfSongsRussian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs__russian);

        String[] songsRus = getResources().getStringArray(R.array.Songs_list_Russian);

        Arrays.asList(songsRus);

        listOfSongsRussian = (ListView) findViewById(R.id.listSongsViewTussian);
        listOfSongsRussian.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_2, songsRus));

        getIndexListOfSongs(songsRus);

        displayIndex();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_songs__russian, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getIndexListOfSongs(String[] songs) {
        mapIndexRus = new LinkedHashMap<String, Integer>();
        for(int i = 0; i < songs.length; i++){
            String song = songs[i];
            String index = song.substring(0, 1);

            if(mapIndexRus.get(index) == null)
                mapIndexRus.put(index, i);
        }
    }

    private void displayIndex(){
        LinearLayout indexLayout = (LinearLayout) findViewById(R.id.side_index_russian);
        TextView textView;

        List<String> indexList = new ArrayList<String>(mapIndexRus.keySet());
        for(String index : indexList){
            textView = (TextView) getLayoutInflater().inflate(R.layout.side_index_item_russian, null);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayout.addView(textView);
        }
    }

    @Override
    public void onClick(View view) {
        TextView selectedIndex = (TextView) view;
        listOfSongsRussian.setSelection(mapIndexRus.get(selectedIndex.getText()));
    }
}
