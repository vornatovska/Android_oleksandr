package ua.oleksandr.guitar;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class Songs_Ukranian extends ActionBarActivity implements OnClickListener {
    Map<String, Integer> mapIndexUkranian;
    ListView listUkranian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs__ukranian);

        String[] songsUa = getResources().getStringArray(R.array.songs_list_Ukranian);

        Arrays.asList(songsUa);

        listUkranian = (ListView) findViewById(R.id.listSongsUkranian);
        listUkranian.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_3, songsUa));

        getIndexListUkranian(songsUa);

        displayInegsUA();
    }

    private void getIndexListUkranian(String[] songsUa){
        mapIndexUkranian = new LinkedHashMap<String, Integer>();
        for(int i = 0; i < songsUa.length; i++){
            String songsU = songsUa[i];
            String indexI = songsU.substring(0, 1);

            if(mapIndexUkranian.get(indexI) == null)
                mapIndexUkranian.put(indexI, i);
        }
    }

    private void displayInegsUA() {
        LinearLayout indexLayoutUA = (LinearLayout) findViewById(R.id.side_list_item_Ukranian);

        TextView textView;

        List<String> indexList = new ArrayList<String>(mapIndexUkranian.keySet());
        for(String index : indexList){
            textView = (TextView) getLayoutInflater().inflate(R.layout.side_index_item_ukranian, null);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayoutUA.addView(textView);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_songs__ukranian, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        TextView selectedIndexUA = (TextView) view;
        listUkranian.setSelection(mapIndexUkranian.get(selectedIndexUA.getText()));

    }
}
