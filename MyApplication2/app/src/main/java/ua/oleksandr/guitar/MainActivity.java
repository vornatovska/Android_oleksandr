package ua.oleksandr.guitar;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends ActionBarActivity  implements View.OnClickListener{
    Random random = new Random();
    private Button buttonChords, buttonSongs, buttonFauvorite, buttoFind;
    private String[] mSayings = {"  My guitar is not a thing. It is an extension of myself. It is who I am. - Joan Jett ", "  Sometimes the nicest thing to do with a guitar is just look at it. - Thom Yorke"
            , "  I just go where the guitar takes me. - Angus Young", "  You couldn't not like someone who liked the guitar. - Stepen King", "  The history of music is mortal, but the idiocy of the guitar is eternal. Milan Kundera", "When I`m having a bad day, I pick up my guitar. - Michelle Branch",
            "  The guitar was my weapon, my shield to hide behind. - Brian May", "  Listening is the key to everything good in music. - Pat Metheny"};
    private TextView mQuoteOut;
    private int lengthOfSaying = random.nextInt(mSayings.length);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonChords = (Button) findViewById(R.id.buttonChords);
        buttonChords.setOnClickListener(this);

        buttonSongs = (Button) findViewById(R.id.buttonSongs);
        buttonSongs.setOnClickListener(this);

        buttonFauvorite = (Button) findViewById(R.id.buttonFauvorite);
        buttonFauvorite.setOnClickListener(this);

        buttoFind = (Button) findViewById(R.id.buttonFind);
        buttoFind.setOnClickListener(this);

        mQuoteOut = (TextView) findViewById(R.id.textForOut);
        mQuoteOut.setText(mSayings[lengthOfSaying]);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonChords:
                startActivity(new Intent(this, Chords_menu.class));
                break;
            case R.id.buttonSongs:
                startActivity(new Intent(this, Select_song_language.class));
                break;
            case R.id.buttonFauvorite:
                startActivity(new Intent(this,Fauvorite_menu.class));
                break;
            case  R.id.buttonFind:
                startActivity(new Intent(this, Find_menu.class));
                break;
        }
    }
}
