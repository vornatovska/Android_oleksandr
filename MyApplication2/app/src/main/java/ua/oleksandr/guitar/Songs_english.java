package ua.oleksandr.guitar;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class Songs_english extends ActionBarActivity implements OnClickListener {
    Map<String, Integer> mapIndex;
    ListView listOfSongsEnglish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs_english);

         String[] songs = getResources().getStringArray(R.array.Songs_list);

        Arrays.asList(songs);

        listOfSongsEnglish = (ListView) findViewById(R.id.listSongsView);
        listOfSongsEnglish.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, songs));

        getIndexListOfSongs(songs);

        displayIndex();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_songs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private  void getIndexListOfSongs(String[] songs){
        mapIndex = new LinkedHashMap<String, Integer>();
        for(int i = 0; i < songs.length; i++){
            String song = songs[i];
            String index = song.substring(0, 1);

            if(mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }

    private void displayIndex(){
        LinearLayout indexLayout = (LinearLayout) findViewById(R.id.side_index);
        TextView textView;

        List<String> indexList = new ArrayList<String>(mapIndex.keySet());
        for(String index : indexList){
            textView = (TextView) getLayoutInflater().inflate(R.layout.side_index_item_english, null);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayout.addView(textView);
        }
    }

    @Override
    public void onClick(View view) {
        TextView selectedIndex = (TextView) view;
        listOfSongsEnglish.setSelection(mapIndex.get(selectedIndex.getText()));
    }
}
