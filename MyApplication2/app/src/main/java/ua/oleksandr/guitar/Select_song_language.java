package ua.oleksandr.guitar;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class Select_song_language extends ActionBarActivity implements View.OnClickListener{
    private Button buttonRessuian, buttonEnglish, buttonUkranian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_song_language);

        buttonEnglish = (Button) findViewById(R.id.buttoEnglish);
        buttonEnglish.setOnClickListener(this);

        buttonRessuian = (Button) findViewById(R.id.buttonRussian);
        buttonRessuian.setOnClickListener(this);

        buttonUkranian = (Button) findViewById(R.id.buttonUkranian);
        buttonUkranian.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_song_language, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttoEnglish:
                startActivity(new Intent(this, Songs_english.class));
                break;
            case  R.id.buttonRussian:
                startActivity(new Intent(this, Songs_Russian.class));
                break;
            case  R.id.buttonUkranian:
                startActivity(new Intent(this, Songs_Ukranian.class));
                break;
        }
    }
}
